# Vulnerability 1: HTML injection in parameter `username`

- Vulnerability: HTML injection
- Where: `username` parameter of register
- Impact: Allows for defacing of the feed page, friends page and pending requests page

## Steps to reproduce

1. Insert `username` = `<h1> Hello </h1>` and password=`anything` in register form
2. Create a new post with any content and any type
3. Check your post you should see `Hello` in big letters

[(POC)](vuln1.py)