# Project1-Group24

Group Image available [here](http://ea40bfc5f8cb1209bc8e82a9fef82e77123a0845ee6a784ba7e386411b01.project.ssof.rnl.tecnico.ulisboa.pt/)

- Vulnerabilities in `/`:

	1. Broken Access Control in feed after another user performs an update which allows you to edit their posts [link](feed_vulns/vuln1.md)

- Vulnerabilities in `/login`:

	1. SQL injection in parameter `username` allows reading the posts and profile of other users [link](login_vulns/vuln1.md)

	2. SQL injection in parameter `password` allows full access to other users' accounts [link](login_vulns/vuln2.md)

- Vulnerabilities in `/register`:

	1. HTML injection in parameter `username` allows for defacing [link](register_vulns/vuln1.md)

	2. SQL injection in parameter `username` allows for execution of any query on the database [link](register_vulns/vuln2.md)

	3. SQL injection in parameter `password` allows for execution of any query on the database [link](register_vulns/vuln3.md)

- Vulnerabilities in `/profile`:
    1. SQL injection in parameters `about`, `name`, `newpassword` and `photo` allow for changing other user's info [link](profile_vulns/vuln1.md)

    2. Stored XSS in parameters `name` and `about` allows for defacing [link](profile_vulns/vuln2.md)

    3. Stored XSS + CSRF in parameters `name` and `about` allows for making requests as another user [link](profile_vulns/vuln3.md)

- Vulnerabilities in `/create_post`:
    1. SQL injection in `content` field allows for execution of any query on the database [link](create_post_vulns/vuln1.md)

    2. Stored XSS in parameter `content` allows for home page defacing [link](create_post_vulns/vuln2.md)

    3. Stored XSS + CSRF in the content of the posts, detailed in [`/profile` #3](profile_vulns/vuln3.md) since they have the same effect

- Vulnerablities in `/edit_post`:

	1. Broken Access Control allows reading of any post [link](edit_post_vulns/vuln1.md)

	2. Broken Access Control allows editing of any post [link](edit_post_vulns/vuln2.md)

	3. SQLi allows editing of all posts [link](edit_post_vulns/vuln3.md)

	4. Stored XSS in the content of the posts, detailed in [`/create_post` #2](create_post_vulns/vuln2.md) since they have the same effect

	5. Stored XSS + CSRF in the content of the posts, detailed in [`/profile` #3](profile_vulns/vuln3.md) since they have the same effect

- Vulnerabilities in `/friends`:

    1. SQL injection in parameter `search` allows to get any information from the database [link](friends_vulns/vuln1.md)

- Vulnerabilities in `/request_friends`:
	1. SQL injection in parameter `username` allows for execution of any query on the database [link](request_friend_vulns/vuln1.md)

- Vulnerabilities in `/pending_requests`:
	1. SQL injection in hidden parameter `username` allows for execution of any query on the database [link](pending_requests_vulns/vuln1.md)
