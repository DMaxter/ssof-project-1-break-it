# Vulnerability 2: SQL injection in parameter `password`

- Vulnerability: SQL injection
- Where: `password` parameter of `/login`
- Impact: Allows full access to the account of a user with a known username

## Steps to reproduce

1. Insert `username` = `ssofadmin` and `password` = `' OR username='ssofadmin` (both `ssofadmin`s can be replaced with the username of the target)
2. You'll be logged in as the target account having full access over it

[(POC)](vuln2.py)

**Note:** The POC only accepts a Friend Request from the attacker, but any action can be taken.

## Other payloads
Using a different username in the `username` parameter and inside the injection results in
a mixed session where some functionalities consider us to be `user1` while others consider
us to be `user2`, but it's less powerfull than taking full control of the account, example:
`user` = `investor`, `password` = `' OR username='ssofadmin`
