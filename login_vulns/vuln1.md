# Vulnerability 1: SQL injection in prameter `username`

- Vulnerability: SQL injection
- Where: `username` parameter of `/login`
- Impact: Allows reading posts and profile of any user with a known username (posts from other users don't appear in the feed when we login in this way)

## Steps to reproduce

1. Insert `username` = `ssofadmin' #` and `password` = `anything` in login form (`ssofadmin` can be replaced with the username of the target)
2. You'll only be able to see the posts and profile of the logged in user, anything else results in SQL errors

**Note:** Since the user's posts are visible they are also editable.

[(POC)](vuln1.py)
