# Vulnerability 1: Loading feed after another user performs an update allows you to edit their posts

- Vulnerability: Broken Access Control
- Where: loading the feed information
- Impact: You can edit other user's public posts and friends posts(if you're friends with him), but not their private ones

## Steps to reproduce

1. Register 2 users `user1` and `user2`(on seperate sessions)
2. On `user1` Create a public post
3. On `user1` update profile
4. On `user2` go to feed(you should now be able to edit `user1`'s post)

[(POC)](vuln1.py)