#!/usr/bin/env python
#
# Editing other users posts from feed

from bs4 import BeautifulSoup
from sys import argv
import requests

SERVER = argv[1]

USERNAME1 = "feeder"
PASSWORD1 = "91v3f33d"

USERNAME2 = "mfeeded"
PASSWORD2 = "93tf33d3d"

NEW_USER1_ABOUT = "I shall feed you"

USER1_POST_CONTENT = "I try so hard not to be feeder"
USER2_POST_CONTENT = "I got feeded"

# Reset server
requests.get(SERVER + "/init")

#Creating sessions for each user
s1 = requests.Session()
s2 = requests.Session()

# Register user 'feeder' and user '.feeded'
r1 = s1.post(SERVER + "/register", data = {
    "username": USERNAME1,
    "password": PASSWORD1,
}, allow_redirects=False)

r2 = s2.post(SERVER + "/register", data = {
    "username": USERNAME2,
    "password": PASSWORD2,
}, allow_redirects=False)

# Check users properly registered
assert r1.status_code == 302
assert r2.status_code == 302

# Check user2's feed does not contain a post he can edit
assert "Edit this post" not in r2.text

# Create a public post on user1
r = s1.post(SERVER + "/create_post", data = {
    "content": USER1_POST_CONTENT,
    "type": "Public",
}, allow_redirects=False)

# Update profile of user1
r = s1.post(SERVER + "/update_profile", data = {
    "currentpassword": PASSWORD1,
    "name": "",
    "newpassword": "",
    "about": NEW_USER1_ABOUT,
}, files = {
    "photo": ""
}, allow_redirects=False)

# Check post properly created
assert r.status_code == 200

# Load feed on user2
r = s2.get(SERVER + "/", allow_redirects=False)

# Check new post and edit button is in user2's feed
assert USER1_POST_CONTENT in r.text
assert "Edit this post" in r.text

html = BeautifulSoup(r.text, "html.parser")
post = html.find_all("input")[0]
post_id = post["value"]

# Change post on user2
r = s2.post(SERVER + "/edit_post", data = {
	"id": post_id,
    "content": USER2_POST_CONTENT,
    "type": "Public",
}, allow_redirects=False)

# Load feed on user1
r = s1.get(SERVER + "/", allow_redirects=False)

# Check post new content on user1
assert USER2_POST_CONTENT in r.text
