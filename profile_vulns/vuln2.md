# Vulnerability 2: Stored XSS in update profile form allows for page defacing

- Vulnerability: Stored XSS
- Where: `name` and `about` fields
- Impact: Allows to change the home page (only the `name` field) and the `My Friends` and `Pending Requests` page (both fields)

## Steps to reproduce

1. Register a user
2. Create a new **Public** Post
3. Go to `Profile(name)`
4. Insert logged user's password on `currentpassword` field
5. Insert `<script>alert(\\\"RICK ROLLED!!!!\\\");location.href=\\\"https://www.youtube.com/watch?v=dQw4w9WgXcQ&autoplay=1\\\";</script>`
6. Go to home page

[(POC)](vuln2.py)
