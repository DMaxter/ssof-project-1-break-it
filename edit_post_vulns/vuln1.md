# Vulnerability 1: Broken Access Control on `GET` allows reading of any post

- Vulnerability: Broken Access Control
- Where: Loading of post for `/edit_post`
- Impact: You can read any post, regardless of author and permission

## Steps to reproduce

1. Register 1 user, `user`
2. For each `i` in `1..N` until error page is returned do:
3. With `user`: `GET` `URL/edit_post?id=i` (you'll be able to see the content of every post)

[(POC)](vuln1.py)

**Note:** The POC creates only 1 post and reads only that one for simplicity
