# Vulnerability 3: SQL Injection on POST form fields allows editing of any post

- Vulnerability: SQL Injection
- Where: `id`, `content` and `type` in `edit_post` form
- Impact: You can edit any post and **all** posts at once, regardless of author and permissions

## Steps to reproduce

1. Register 1 user, `user`
2. `POST` to `/edit_post` with the Form Data fields: `id` = `any`, `content` = `new text', type='Public', author='ssofadmin' #` and , `type` = `any`
4. `GET` `URL/` (all posts should now have the `content` 'new text' and be public)

[(POC)](vuln3.py)

## Other payloads

Adding `, created_at = '2020-10-22'` or `, updated_at = '2020-10-22'` to the `content` payload can change those fields, but it doesn't seem to have any effect.

Fields `id` and `type` are also vulnerable to SQLi, but the [(POC)](vuln3.py) (which uses the injection in `content`) is more impactful.

We can choose to not change the `author` or `type` fields to make it less obvious that we've attacked the service.
Or add a `where` clause to the injected SQL so that only certain posts are changed, like a given user's. (` where author = 'ssofadmin' # `)
