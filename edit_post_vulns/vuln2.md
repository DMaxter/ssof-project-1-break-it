# Vulnerability 2: Broken Access Control on POST allows editing of any post

- Vulnerability: Broken Access Control
- Where: Editing of post
- Impact: You can edit any post, regardless of author and permission

## Steps to reproduce

1. Register 1 user, `user`
2. For each `i` in `1..N` until error page is returned do:
3. With `user`: `POST` to `URL/edit_post` with the Form Data field `id` set to `i` (you can choose any `content`, `type` must be one of the valid three, `"Public"` is recommended to test exploit)
4. `GET` `URL/` (all posts should now have the chosen `content` and be public)

[(POC)](vuln2.py)

**Note:** The POC creates only 1 post and edits only that one for simplicity

